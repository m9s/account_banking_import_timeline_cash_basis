#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
"Batch Import Line"
import copy
from trytond.model import ModelView, ModelSQL
from trytond.pyson import Bool, Eval, PYSONEncoder
from trytond.pool import Pool


class Line(ModelSQL, ModelView):
    _name = 'account.batch.import.line'

    #def __init__(self):
    #    super(Line, self).__init__()

    #    # tax
    #    self.tax = copy.copy(self.tax)
    #    if 'readonly' in self.tax.states:
    #        if (PYSONEncoder().encode(self.tax.states['readonly'])
    #                == PYSONEncoder().encode(Bool(Eval('invoice')))):
    #            self.tax.states['readonly'] = False
    #    if 'invoice' not in self.tax.depends:
    #        self.tax.depends = copy.copy(self.tax.depends)
    #        self.tax.depends.append('invoice')
    #    ### TODO: Field "fiscalyear" doesn\'t exist on "account.tax"
    #    ### This tax domain depending on taxation_method is difficult to implement
    #    #tax_domain = [
    #    #    ('fiscalyear.taxation_method', '=', 'cash_income'),
    #    #    ('payment_tax', If(Bool(Eval('invoice')), '=', '!='), True),
    #    #    ]
    #    #if self.tax.domain is None:
    #    #    self.tax.domain = tax_domain
    #    #else:
    #    #    self.tax.domain += tax_domain

    #    self._reset_columns()

    def on_change_contra_account(self, vals):
        res = super(Line, self).on_change_contra_account(vals)
        res['tax'] = self._get_invoice_due_tax(vals)
        return res

    def on_change_invoice(self, vals):
        res = super(Line, self).on_change_invoice(vals)
        res['tax'] = self._get_invoice_due_tax(vals)
        return res

    def _get_invoice_due_tax(self, vals):
        pool = Pool()
        fiscalyear_obj = pool.get('account.fiscalyear')
        invoice_tax_obj = pool.get('account.invoice.tax')

        res = False
        fiscalyear = fiscalyear_obj.get_fiscalyear(vals.get('date'))
        if fiscalyear.taxation_method == 'cash_income':
            invoice_id = vals.get('invoice')
            if invoice_id:
                invoice_tax_id = invoice_tax_obj.search([
                        ('invoice', '=', invoice_id)])
                invoice_tax = invoice_tax_obj.browse(invoice_tax_id)[0]
                res = invoice_tax.tax.due_tax.id
        return res

Line()
