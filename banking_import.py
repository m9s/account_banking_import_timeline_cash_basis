# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL
from trytond.pool import Pool


class BankingImportLine(ModelSQL, ModelView):
    _name = 'banking.import.line'

    def _get_tax(self, invoice_id, date):
        res = super(BankingImportLine, self)._get_tax(invoice_id, date)
        pool = Pool()
        fiscalyear_obj = pool.get('account.fiscalyear')
        invoice_tax_obj = pool.get('account.invoice.tax')

        res = False
        #print vals
        fiscalyear = fiscalyear_obj.get_fiscalyear(date)
        if fiscalyear.taxation_method == 'cash_income':
            invoice_tax_id = invoice_tax_obj.search([
                    ('invoice', '=', invoice_id)])
            print invoice_tax_id
            invoice_tax = invoice_tax_obj.browse(invoice_tax_id)[0]
            res = invoice_tax.tax.due_tax.id
        print res
        return res

BankingImportLine()
