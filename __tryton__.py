# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Banking Import Timeline Cash Basis',
    'name_de_DE': 'Buchhaltung Bankimport Gültigkeitsdauer Ist-Versteuerung',
    'version': '2.2.0',
    'author': 'MBSolutions',
    'email': 'info@m9s.biz',
    'website': 'http://www.m9s.biz',
    'description': '''Cash Basis for Timeline Bank Imports
    - Adds cach basis features to the Banking Import Timeline Module
''',
    'description_de_DE': '''Ist-Versteuerung für Gültigkeitsdauer Bankimport
    - Fügt die Merkmale für Ist-Versteuerung den Gültigkeitsdauer-Modulen
    für Bankimport hinzu
''',
    'depends': [
        'account_banking_import_timeline',
        ],
    'xml': [
    ],
    'translation': [
    ],
}
